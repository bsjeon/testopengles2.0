//
//  NormalCircle.cpp
//  TestOpenGLES2.0
//
//  Created by Jeon ByungSoo on 13. 6. 13..
//
//

#include "NormalCircle.h"


NormalCircle::NormalCircle()
{
    
}

NormalCircle::~NormalCircle()
{
    
}

bool NormalCircle::init()
{
    if (!CCNode::init()) {
        return false;
    }
    /**
     Compile Shader Program for anti-aliased Circle
     **/
    ///////////////////////////////////////////////////////////////////////////////////////
    
    const std::string fileP = CCFileUtils::sharedFileUtils()->fullPathForFilename("circleVert3.vsh");
    CCString *string = CCString::createWithContentsOfFile(fileP.c_str());
    const GLchar * vertSource = string->getCString();
    
    const std::string fileP2 = CCFileUtils::sharedFileUtils()->fullPathForFilename("circleFrag3.fsh");
    CCString *string2 = CCString::createWithContentsOfFile(fileP2.c_str());
    const GLchar * fragSource = string2->getCString();
    
    m_program = new CCGLProgram();
    m_program->initWithVertexShaderByteArray(vertSource, fragSource);
    m_program->link();
    m_program->updateUniforms();
    
    ///////////////////////////////////////////////////////////////////////////////////////
    
    /**
     Set Attribute & Parameter for shader
     **/
    ///////////////////////////////////////////////////////////////////////////////////////
    
    m_program->addAttribute(kVertexPosition, kVertexPosition_Idx);
    
    m_borderPos = m_program->getUniformLocationForName(kBorder);
    m_radiusPos = m_program->getUniformLocationForName(kRadius);
    m_color1Pos = m_program->getUniformLocationForName(kColor1);
    m_centerPos = m_program->getUniformLocationForName(kCenter);
    
    m_radius = SCREEN_H/2;
    m_border = m_radius/20.0f;
    m_color1 = ccc4FFromccc3B(ccRED);
    m_center = vertex2(SCREEN_W/2, SCREEN_H/2);
    
    //    CCLog("Vertex : %d", kVertexPosition_Idx);
    
    ///////////////////////////////////////////////////////////////////////////////////////
    
    /**
     Set Vertices Coordinates (rect which exactly includes circle)
     **/
    ///////////////////////////////////////////////////////////////////////////////////////
    
    CCRect circleRegion = CCRectMake(m_center.x - m_radius, m_center.y - m_radius,
                                     m_radius*2, m_radius*2);
    
    m_vertices.push_back(vertex2(circleRegion.getMinX(), circleRegion.getMinY()));
    m_vertices.push_back(vertex2(circleRegion.getMinX(), circleRegion.getMaxY()));
    m_vertices.push_back(vertex2(circleRegion.getMaxX(), circleRegion.getMinY()));
    m_vertices.push_back(vertex2(circleRegion.getMaxX(), circleRegion.getMaxY()));

    return true;
}

void NormalCircle::draw()
{
    glClearColor(0, 0, 0, 1);
    
    setShaderProgram(m_program);
    getShaderProgram()->use();
    
    CC_NODE_DRAW_SETUP();
    
    glEnableVertexAttribArray(kVertexPosition_Idx);
    
    /* Update Parameters */
    glUniform1f(m_borderPos, m_border);
    glUniform1f(m_radiusPos, m_radius);
    glUniform4fv(m_color1Pos, 1, (GLfloat *)&m_color1);
    glUniform2fv(m_centerPos, 1, (GLfloat *)&m_center);
    
    glVertexAttribPointer(kVertexPosition_Idx, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid *) &m_vertices[0]);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei) m_vertices.size());

}