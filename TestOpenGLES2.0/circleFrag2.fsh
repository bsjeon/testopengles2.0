varying highp vec2 textureCoordinate;

const highp vec2 center = vec2(0.5, 0.5);
const highp float radius = 0.5;

void main()
{
    highp float distanceFromCenter = distance(center, textureCoordinate);
    highp float checkForPresenceWithinCircle = step(distanceFromCenter, radius);
    
    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0) * checkForPresenceWithinCircle;
}
