uniform float border;
uniform float radius;
uniform vec4 color1;
uniform vec2 center;

varying vec2 uv;

void main() {
    vec4 color0 = vec4(0.0, 0.0, 0.0, 0.0);

    vec2 m = uv - center;
    float dist = radius - sqrt(m.x * m.x + m.y * m.y);
    
    float t = 0.0;
    if (dist > border)
    t = 1.0;
    else if (dist > 0.0)
    t = dist / border;
    
    gl_FragColor = mix(color0, color1, t);
}