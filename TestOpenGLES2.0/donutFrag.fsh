uniform float border;
uniform float radius;
uniform float inRadius;
uniform vec4 color1;
uniform vec4 colorIn;
uniform vec2 center;

varying vec2 uv;

void main() {
    vec4 color0 = vec4(0.0, 0.0, 0.0, 0.0);
  vec4 finalColor0 = color0;
  vec4 finalColor = colorIn;
    vec2 m = uv - center;

  float fragDist = sqrt(m.x * m.x + m.y * m.y);
  float inDist = inRadius - fragDist;
    float dist = radius - fragDist;

  float t;

  if (inDist > 0.0) {
    if (inDist > border) t = 1.0;
    else {
        t = inDist/border;
        finalColor0 = color1;
    }
  } else {
    if (dist > border) t = 1.0;
    else if (dist > 0.0) t = dist/border;
    else t = 0.0;
    finalColor = color1;
  }

  gl_FragColor = mix(finalColor0, finalColor, t);
}