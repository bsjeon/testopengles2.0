//
//  NormalCircle.h
//  TestOpenGLES2.0
//
//  Created by Jeon ByungSoo on 13. 6. 13..
//
//

#ifndef __TestOpenGLES2_0__NormalCircle__
#define __TestOpenGLES2_0__NormalCircle__

#include "cocos2d.h"
#include "Constant.h"

USING_NS_CC;
using namespace std;

#define kVertexPosition "position"

#define kBorder "border"
#define kRadius "radius"
#define kColor1 "color1"
#define kCenter "center"

class NormalCircle : public cocos2d::CCNode
{
private:
    enum {
        kVertexPosition_Idx,
    };
    
    cocos2d::CCGLProgram *m_program;
    
    GLuint m_borderPos;
    GLuint m_radiusPos;
    GLuint m_color1Pos;
    GLuint m_centerPos;
    
    float m_border;
    float m_radius;
    cocos2d::ccColor4F m_color1;
    cocos2d::ccVertex2F m_center;
    
    std::vector<cocos2d::ccVertex2F> m_vertices;

    virtual void draw();
    
public:
    NormalCircle();
    virtual ~NormalCircle();
    
    CREATE_FUNC(NormalCircle);
    bool init();
};
#endif /* defined(__TestOpenGLES2_0__NormalCircle__) */
