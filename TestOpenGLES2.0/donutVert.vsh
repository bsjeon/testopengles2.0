attribute vec4 position;

varying vec2 uv;

void main() {
    gl_Position = CC_MVPMatrix * position;
    uv = vec2(position);
}