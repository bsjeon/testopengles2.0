//
//  Constant.h
//  TestOpenGLES2.0
//
//  Created by Jeon ByungSoo on 13. 6. 13..
//
//

#ifndef TestOpenGLES2_0_Constant_h
#define TestOpenGLES2_0_Constant_h

#define SCREEN_W cocos2d::CCDirector::sharedDirector()->getWinSize().width
#define SCREEN_H cocos2d::CCDirector::sharedDirector()->getWinSize().height

#endif
