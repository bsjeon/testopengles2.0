#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "NormalCircle.h"
#include "DonutCircle.h"

using namespace cocos2d;
using namespace CocosDenshion;
using namespace std;

CCScene* HelloWorld::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    HelloWorld *layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    DonutCircle *donut = DonutCircle::create();
    donut->setPosition(CCPointZero);
    donut->setAnchorPoint(CCPointZero);
    this->addChild(donut);
    
//    NormalCircle *circle = NormalCircle::create();
//    circle->setPosition(CCPointZero);
//    circle->setAnchorPoint(CCPointZero);
//    this->addChild(circle);
    
    return true;
}