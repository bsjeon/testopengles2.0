uniform float uThickness;

attribute vec3 aPosition;
attribute vec2 aTexCoord;
attribute vec4 aColor;

varying vec4 vColor;

void main() {
    vec4 position = CC_MVPMatrix * vec4(aPosition.xyz, 1.);
    vColor = aColor;
    gl_PointSize = uThickness;
    gl_Position =  position;
}